(function() {
  'use strict';

  angular
    .module('chatbot', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'restangular', 'ui.router', 'toastr']);

})();
