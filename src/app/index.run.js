(function() {
  'use strict';

  angular
    .module('chatbot')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
