(function() {
  'use strict';

  angular
    .module('chatbot')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
	$stateProvider
	    .state('chat', {
	      url: '/',
	      templateUrl: 'app/main/templates/chat.html',
	      controller: 'ChatController',
	      controllerAs: 'chat'
	    });
	$urlRouterProvider.otherwise('/');
  }

})();
